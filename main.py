import pyglet
from game import resources

game_window = pyglet.window.Window(800, 600)


player = pyglet.sprite.Sprite(img=resources.player_image, x=400, y=300)

#todo : Continue here https://pyglet.readthedocs.io/en/latest/programming_guide/examplegame.html


@game_window.event
def on_draw():
    game_window.clear()

if __name__ == '__main__':
    pyglet.app_run()
