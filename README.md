# Duality
_An game write in python_

**Duality Rules**
___
• Dans Duality, les combats s’effectuent en un contre un. Pour en finir avec votre adversaire, vous devez lui retirer les trois vies qu’il dispose. 

• Ca ne va pas être une partie de plaisir, car pour se faire vous allez incarner un puissant héros qui peut voler, afin de battre votre adversaire utilisez le vole parfaitement en appuyant deux fois sur (Touche) cela vous permettra d’avoir un avantage si vous n’utilisez pas votre énergie n’importe comment, si votre énergie tombe à 0, vous ne pouvez plus voler. Restez au sol pour pouvoir recharger votre énergie !

• Aidez-vous de votre arme ! Vos attaques de base (Touche), vous permet d’établir des combos dévastateurs. Votre attaque de base s’effectue en plusieurs coups dans un roulement successif, un cycle. Cependant, vous pouvez arrêter à tout moment ce cycle pour garder de côté une attaque de base intéressante. Je m’explique, votre héros enchaîne les attaques de bases, si vous avez quatre attaques, vous pouvez, par exemple, attaquer deux fois et garder de côté votre troisième attaque. Puis attendre un meilleur moment, une faille de l’adversaire pour l’utiliser. Ensuite, vous renchaînez avec la quatrième attaque et ainsi de suite.

• Dans votre combat acharné, votre héros ne sait pas que voler ni attaquer avec son arme, il vous offre la possibilité d’utiliser de puissant pouvoir. Cinq, pour être plus précis, deux pouvoirs qui se changent selon votre position, si vous êtes en l’air ou si vous êtes au sol, placé sur (Touche). Deux autres pouvoirs qui sont statiques, placé sur (Touche). Et une capacité ultime qui se charge en fonction des dégâts que vous mettez à l’adversaire. (Touche)
Faîtes attention à bien gérer vos capacités ! Il vous faut attendre avant de pouvoir les réutiliser !


• Et voilà vous savez tout ! Maitenant allez botter les fesses de cette enflure !

