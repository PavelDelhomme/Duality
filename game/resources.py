from pyglet import resource

resource.path = ['../assets']
pyglet.resource.reindex()


# centering images
def center_image(image):
    image.anchor_x = image.width // 2
    image.anchor_y = image.height // 2
